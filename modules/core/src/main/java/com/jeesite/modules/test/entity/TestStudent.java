/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.entity;

import org.hibernate.validator.constraints.Length;
import java.util.List;
import com.jeesite.common.collect.ListUtils;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 学生测试表Entity
 * @author xujun
 * @version 2024-02-20
 */
@Table(name="test_student", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="idcard", attrName="身份证", label="idcard"),
		@Column(name="name", attrName="姓名", label="name", queryType=QueryType.LIKE),
		@Column(name="password", attrName="密码", label="password"),
	}, orderBy="a.id DESC"
)
public class TestStudent extends DataEntity<TestStudent> {
	
	private static final long serialVersionUID = 1L;
	private String 身份证;		// idcard
	private String 姓名;		// name
	private String 密码;		// password
	private List<TestStudentBk> testStudentBkList = ListUtils.newArrayList();		// 子表列表
	
	public TestStudent() {
		this(null);
	}

	public TestStudent(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="idcard长度不能超过 255 个字符")
	public String get身份证() {
		return 身份证;
	}

	public void set身份证(String 身份证) {
		this.身份证 = 身份证;
	}
	
	@Length(min=0, max=255, message="name长度不能超过 255 个字符")
	public String get姓名() {
		return 姓名;
	}

	public void set姓名(String 姓名) {
		this.姓名 = 姓名;
	}
	
	@Length(min=0, max=255, message="password长度不能超过 255 个字符")
	public String get密码() {
		return 密码;
	}

	public void set密码(String 密码) {
		this.密码 = 密码;
	}
	
	public List<TestStudentBk> getTestStudentBkList() {
		return testStudentBkList;
	}

	public void setTestStudentBkList(List<TestStudentBk> testStudentBkList) {
		this.testStudentBkList = testStudentBkList;
	}
	
}