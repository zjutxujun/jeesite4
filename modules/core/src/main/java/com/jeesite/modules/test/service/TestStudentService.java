/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.test.entity.TestStudent;
import com.jeesite.modules.test.dao.TestStudentDao;
import com.jeesite.modules.test.entity.TestStudentBk;
import com.jeesite.modules.test.dao.TestStudentBkDao;

/**
 * 学生测试表Service
 * @author xujun
 * @version 2024-02-20
 */
@Service
@Transactional(readOnly=true)
public class TestStudentService extends CrudService<TestStudentDao, TestStudent> {
	
	@Autowired
	private TestStudentBkDao testStudentBkDao;
	
	/**
	 * 获取单条数据
	 * @param testStudent
	 * @return
	 */
	@Override
	public TestStudent get(TestStudent testStudent) {
		TestStudent entity = super.get(testStudent);
		if (entity != null){
			TestStudentBk testStudentBk = new TestStudentBk(entity);
			testStudentBk.setStatus(TestStudentBk.STATUS_NORMAL);
			entity.setTestStudentBkList(testStudentBkDao.findList(testStudentBk));
		}
		return entity;
	}
	
	/**
	 * 查询分页数据
	 * @param testStudent 查询条件
	 * @param testStudent.page 分页对象
	 * @return
	 */
	@Override
	public Page<TestStudent> findPage(TestStudent testStudent) {
		return super.findPage(testStudent);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param testStudent
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(TestStudent testStudent) {
		super.save(testStudent);
		// 保存 TestStudent子表
		for (TestStudentBk testStudentBk : testStudent.getTestStudentBkList()){
			if (!TestStudentBk.STATUS_DELETE.equals(testStudentBk.getStatus())){
				testStudentBk.set学生(testStudent);
				if (testStudentBk.getIsNewRecord()){
					testStudentBkDao.insert(testStudentBk);
				}else{
					testStudentBkDao.update(testStudentBk);
				}
			}else{
				testStudentBkDao.delete(testStudentBk);
			}
		}
	}
	
	/**
	 * 更新状态
	 * @param testStudent
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(TestStudent testStudent) {
		super.updateStatus(testStudent);
	}
	
	/**
	 * 删除数据
	 * @param testStudent
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(TestStudent testStudent) {
		super.delete(testStudent);
		TestStudentBk testStudentBk = new TestStudentBk();
		testStudentBk.set学生(testStudent);
		testStudentBkDao.deleteByEntity(testStudentBk);
	}
	
}