/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.entity;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * 学生测试表Entity
 * @author xujun
 * @version 2024-02-20
 */
@Table(name="test_student_bk", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="st_id", attrName="学生.id", label="学生id"),
		@Column(name="bk_name", attrName="报考类别", label="报考类别", queryType=QueryType.LIKE),
	}, orderBy="a.id ASC"
)
public class TestStudentBk extends DataEntity<TestStudentBk> {
	
	private static final long serialVersionUID = 1L;
	private TestStudent 学生;		// 学生id 父类
	private String 报考类别;		// 报考类别
	
	public TestStudentBk() {
		this(null);
	}


	public TestStudentBk(TestStudent 学生){
		this.学生 = 学生;
	}
	
	@NotBlank(message="学生id不能为空")
	@Length(min=0, max=255, message="学生id长度不能超过 255 个字符")
	public TestStudent get学生() {
		return 学生;
	}

	public void set学生(TestStudent 学生) {
		this.学生 = 学生;
	}
	
	@NotBlank(message="报考类别不能为空")
	@Length(min=0, max=255, message="报考类别长度不能超过 255 个字符")
	public String get报考类别() {
		return 报考类别;
	}

	public void set报考类别(String 报考类别) {
		this.报考类别 = 报考类别;
	}
	
}