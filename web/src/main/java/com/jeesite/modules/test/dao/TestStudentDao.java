/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.test.entity.TestStudent;

/**
 * 学生测试表DAO接口
 * @author xujun
 * @version 2024-02-22
 */
@MyBatisDao
public interface TestStudentDao extends CrudDao<TestStudent> {
	
}