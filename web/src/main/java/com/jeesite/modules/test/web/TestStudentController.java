/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.test.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.test.entity.TestStudent;
import com.jeesite.modules.test.service.TestStudentService;

/**
 * 学生测试表Controller
 * @author xujun
 * @version 2024-02-22
 */
@Controller
@RequestMapping(value = "${adminPath}/test/testStudent")
public class TestStudentController extends BaseController {

	@Autowired
	private TestStudentService testStudentService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public TestStudent get(String id, boolean isNewRecord) {
		return testStudentService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("test:testStudent:view")
	@RequestMapping(value = {"list", ""})
	public String list(TestStudent testStudent, Model model) {
		model.addAttribute("testStudent", testStudent);
		return "modules/test/testStudentList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("test:testStudent:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<TestStudent> listData(TestStudent testStudent, HttpServletRequest request, HttpServletResponse response) {
		testStudent.setPage(new Page<>(request, response));
		Page<TestStudent> page = testStudentService.findPage(testStudent);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("test:testStudent:view")
	@RequestMapping(value = "form")
	public String form(TestStudent testStudent, Model model) {
		model.addAttribute("testStudent", testStudent);
		return "modules/test/testStudentForm";
	}

	/**
	 * 保存学生测试表
	 */
	@RequiresPermissions("test:testStudent:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated TestStudent testStudent) {
		testStudentService.save(testStudent);
		return renderResult(Global.TRUE, text("保存学生测试表成功！"));
	}
	
	/**
	 * 删除学生测试表
	 */
	@RequiresPermissions("test:testStudent:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(TestStudent testStudent) {
		testStudentService.delete(testStudent);
		return renderResult(Global.TRUE, text("删除学生测试表成功！"));
	}
	
}