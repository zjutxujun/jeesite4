package com.jeesite.modules.test.web;

import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.test.entity.TestData;
import com.jeesite.modules.test.service.TestDataService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Controller
@RequestMapping(value = "/test")
public class TestController  extends BaseController {
    @Resource
    private TestDataService testDataService;

    @RequestMapping(value = "listData")
    @ResponseBody
    public Page<TestData> listData(TestData testData, HttpServletRequest request, HttpServletResponse response) {
        testData.setPage(new Page<>(request, response));
        Page<TestData> page = testDataService.findPage(testData);
        return page;
    }
}
