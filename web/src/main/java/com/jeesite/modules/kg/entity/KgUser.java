/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.entity;

import org.hibernate.validator.constraints.Length;
import java.util.Date;
import com.jeesite.common.mybatis.annotation.JoinTable;
import com.jeesite.common.mybatis.annotation.JoinTable.Type;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.List;
import com.jeesite.common.collect.ListUtils;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * kg_userEntity
 * @author XJ
 * @version 2024-02-22
 */
@Table(name="kg_user", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="name", attrName="name", label="name", queryType=QueryType.LIKE),
		@Column(name="icon", attrName="icon", label="icon"),
		@Column(name="openid", attrName="openid", label="openid"),
		@Column(name="mobile", attrName="mobile", label="mobile"),
		@Column(name="email", attrName="email", label="email"),
		@Column(name="status", attrName="status", label="状态", comment="状态（0正常 1删除 2停用）", isUpdate=false),
		@Column(name="reg_time", attrName="regTime", label="reg_time"),
		@Column(name="act_time", attrName="actTime", label="act_time"),
	}, orderBy="a.id DESC"
)
public class KgUser extends DataEntity<KgUser> {
	
	private static final long serialVersionUID = 1L;
	private String name;		// name
	private String icon;		// icon
	private String openid;		// openid
	private String mobile;		// mobile
	private String email;		// email
	private Date regTime;		// reg_time
	private Date actTime;		// act_time
	private List<KgUserColl> kgUserCollList = ListUtils.newArrayList();		// 子表列表
	
	public KgUser() {
		this(null);
	}

	public KgUser(String id){
		super(id);
	}
	
	@Length(min=0, max=100, message="name长度不能超过 100 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=100, message="icon长度不能超过 100 个字符")
	public String getIcon() {
		return icon;
	}

	public void setIcon(String icon) {
		this.icon = icon;
	}
	
	@Length(min=0, max=100, message="openid长度不能超过 100 个字符")
	public String getOpenid() {
		return openid;
	}

	public void setOpenid(String openid) {
		this.openid = openid;
	}
	
	@Length(min=0, max=100, message="mobile长度不能超过 100 个字符")
	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}
	
	@Length(min=0, max=100, message="email长度不能超过 100 个字符")
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getRegTime() {
		return regTime;
	}

	public void setRegTime(Date regTime) {
		this.regTime = regTime;
	}
	
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	public Date getActTime() {
		return actTime;
	}

	public void setActTime(Date actTime) {
		this.actTime = actTime;
	}
	
	public Date getRegTime_gte() {
		return sqlMap.getWhere().getValue("reg_time", QueryType.GTE);
	}

	public void setRegTime_gte(Date regTime) {
		sqlMap.getWhere().and("reg_time", QueryType.GTE, regTime);
	}
	
	public Date getRegTime_lte() {
		return sqlMap.getWhere().getValue("reg_time", QueryType.LTE);
	}

	public void setRegTime_lte(Date regTime) {
		sqlMap.getWhere().and("reg_time", QueryType.LTE, regTime);
	}
	
	public Date getActTime_gte() {
		return sqlMap.getWhere().getValue("act_time", QueryType.GTE);
	}

	public void setActTime_gte(Date actTime) {
		sqlMap.getWhere().and("act_time", QueryType.GTE, actTime);
	}
	
	public Date getActTime_lte() {
		return sqlMap.getWhere().getValue("act_time", QueryType.LTE);
	}

	public void setActTime_lte(Date actTime) {
		sqlMap.getWhere().and("act_time", QueryType.LTE, actTime);
	}
	
	public List<KgUserColl> getKgUserCollList() {
		return kgUserCollList;
	}

	public void setKgUserCollList(List<KgUserColl> kgUserCollList) {
		this.kgUserCollList = kgUserCollList;
	}
	
}