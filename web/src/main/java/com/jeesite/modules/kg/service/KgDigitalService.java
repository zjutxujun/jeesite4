/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.kg.entity.KgDigital;
import com.jeesite.modules.kg.dao.KgDigitalDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * kg_digitalService
 * @author XJ
 * @version 2024-02-22
 */
@Service
@Transactional(readOnly=true)
public class KgDigitalService extends CrudService<KgDigitalDao, KgDigital> {
	
	/**
	 * 获取单条数据
	 * @param kgDigital
	 * @return
	 */
	@Override
	public KgDigital get(KgDigital kgDigital) {
		return super.get(kgDigital);
	}
	
	/**
	 * 查询分页数据
	 * @param kgDigital 查询条件
	 * @param kgDigital.page 分页对象
	 * @return
	 */
	@Override
	public Page<KgDigital> findPage(KgDigital kgDigital) {
		return super.findPage(kgDigital);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param kgDigital
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(KgDigital kgDigital) {
		super.save(kgDigital);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(kgDigital.getId(), "kgDigital_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(kgDigital.getId(), "kgDigital_file");
	}
	
	/**
	 * 更新状态
	 * @param kgDigital
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(KgDigital kgDigital) {
		super.updateStatus(kgDigital);
	}
	
	/**
	 * 删除数据
	 * @param kgDigital
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(KgDigital kgDigital) {
		super.delete(kgDigital);
	}
	
}