/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.kg.entity.KgTyword;
import com.jeesite.modules.kg.service.KgTywordService;

/**
 * kg_tywordController
 * @author XJ
 * @version 2024-02-25
 */
@Controller
@RequestMapping(value = "${adminPath}/kg/kgTyword")
public class KgTywordController extends BaseController {

	@Autowired
	private KgTywordService kgTywordService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public KgTyword get(String id, boolean isNewRecord) {
		return kgTywordService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("kg:kgTyword:view")
	@RequestMapping(value = {"list", ""})
	public String list(KgTyword kgTyword, Model model) {
		model.addAttribute("kgTyword", kgTyword);
		return "modules/kg/kgTywordList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("kg:kgTyword:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<KgTyword> listData(KgTyword kgTyword, HttpServletRequest request, HttpServletResponse response) {
		kgTyword.setPage(new Page<>(request, response));
		Page<KgTyword> page = kgTywordService.findPage(kgTyword);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("kg:kgTyword:view")
	@RequestMapping(value = "form")
	public String form(KgTyword kgTyword, Model model) {
		model.addAttribute("kgTyword", kgTyword);
		return "modules/kg/kgTywordForm";
	}

	/**
	 * 保存同义词近义词
	 */
	@RequiresPermissions("kg:kgTyword:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated KgTyword kgTyword) {
		kgTywordService.save(kgTyword);
		return renderResult(Global.TRUE, text("保存同义词近义词成功！"));
	}
	
	/**
	 * 删除同义词近义词
	 */
	@RequiresPermissions("kg:kgTyword:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(KgTyword kgTyword) {
		kgTywordService.delete(kgTyword);
		return renderResult(Global.TRUE, text("删除同义词近义词成功！"));
	}
	
}