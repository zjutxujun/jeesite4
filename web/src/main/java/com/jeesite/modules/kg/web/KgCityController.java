/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.kg.entity.KgCity;
import com.jeesite.modules.kg.service.KgCityService;

/**
 * kg_cityController
 * @author XJ
 * @version 2024-02-22
 */
@Controller
@RequestMapping(value = "${adminPath}/kg/kgCity")
public class KgCityController extends BaseController {

	@Autowired
	private KgCityService kgCityService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public KgCity get(String nodeid, boolean isNewRecord) {
		return kgCityService.get(nodeid, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("kg:kgCity:view")
	@RequestMapping(value = {"list", ""})
	public String list(KgCity kgCity, Model model) {
		model.addAttribute("kgCity", kgCity);
		return "modules/kg/kgCityList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("kg:kgCity:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<KgCity> listData(KgCity kgCity, HttpServletRequest request, HttpServletResponse response) {
		kgCity.setPage(new Page<>(request, response));
		Page<KgCity> page = kgCityService.findPage(kgCity);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("kg:kgCity:view")
	@RequestMapping(value = "form")
	public String form(KgCity kgCity, Model model) {
		model.addAttribute("kgCity", kgCity);
		return "modules/kg/kgCityForm";
	}

	/**
	 * 保存kg_city
	 */
	@RequiresPermissions("kg:kgCity:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated KgCity kgCity) {
		kgCityService.save(kgCity);
		return renderResult(Global.TRUE, text("保存kg_city成功！"));
	}
	
	/**
	 * 删除kg_city
	 */
	@RequiresPermissions("kg:kgCity:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(KgCity kgCity) {
		kgCityService.delete(kgCity);
		return renderResult(Global.TRUE, text("删除kg_city成功！"));
	}
	
}