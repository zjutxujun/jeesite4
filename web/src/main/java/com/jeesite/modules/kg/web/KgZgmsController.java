/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.kg.entity.KgZgms;
import com.jeesite.modules.kg.service.KgZgmsService;

/**
 * kg_zgmsController
 * @author XJ
 * @version 2024-02-22
 */
@Controller
@RequestMapping(value = "${adminPath}/kg/kgZgms")
public class KgZgmsController extends BaseController {

	@Autowired
	private KgZgmsService kgZgmsService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public KgZgms get(String id, boolean isNewRecord) {
		return kgZgmsService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("kg:kgZgms:view")
	@RequestMapping(value = {"list", ""})
	public String list(KgZgms kgZgms, Model model) {
		model.addAttribute("kgZgms", kgZgms);
		return "modules/kg/kgZgmsList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("kg:kgZgms:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<KgZgms> listData(KgZgms kgZgms, HttpServletRequest request, HttpServletResponse response) {
		kgZgms.setPage(new Page<>(request, response));
		Page<KgZgms> page = kgZgmsService.findPage(kgZgms);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("kg:kgZgms:view")
	@RequestMapping(value = "form")
	public String form(KgZgms kgZgms, Model model) {
		model.addAttribute("kgZgms", kgZgms);
		return "modules/kg/kgZgmsForm";
	}

	/**
	 * 保存kg_zgms
	 */
	@RequiresPermissions("kg:kgZgms:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated KgZgms kgZgms) {
		kgZgmsService.save(kgZgms);
		return renderResult(Global.TRUE, text("保存kg_zgms成功！"));
	}
	
	/**
	 * 停用kg_zgms
	 */
	@RequiresPermissions("kg:kgZgms:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(KgZgms kgZgms) {
		kgZgms.setStatus(KgZgms.STATUS_DISABLE);
		kgZgmsService.updateStatus(kgZgms);
		return renderResult(Global.TRUE, text("停用kg_zgms成功"));
	}
	
	/**
	 * 启用kg_zgms
	 */
	@RequiresPermissions("kg:kgZgms:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(KgZgms kgZgms) {
		kgZgms.setStatus(KgZgms.STATUS_NORMAL);
		kgZgmsService.updateStatus(kgZgms);
		return renderResult(Global.TRUE, text("启用kg_zgms成功"));
	}
	
	/**
	 * 删除kg_zgms
	 */
	@RequiresPermissions("kg:kgZgms:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(KgZgms kgZgms) {
		kgZgmsService.delete(kgZgms);
		return renderResult(Global.TRUE, text("删除kg_zgms成功！"));
	}
	
}