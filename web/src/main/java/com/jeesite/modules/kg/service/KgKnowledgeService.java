/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.kg.entity.KgKnowledge;
import com.jeesite.modules.kg.dao.KgKnowledgeDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * kg_knowledgeService
 * @author XJ
 * @version 2024-02-22
 */
@Service
@Transactional(readOnly=true)
public class KgKnowledgeService extends CrudService<KgKnowledgeDao, KgKnowledge> {
	
	/**
	 * 获取单条数据
	 * @param kgKnowledge
	 * @return
	 */
	@Override
	public KgKnowledge get(KgKnowledge kgKnowledge) {
		return super.get(kgKnowledge);
	}
	
	/**
	 * 查询分页数据
	 * @param kgKnowledge 查询条件
	 * @param kgKnowledge.page 分页对象
	 * @return
	 */
	@Override
	public Page<KgKnowledge> findPage(KgKnowledge kgKnowledge) {
		return super.findPage(kgKnowledge);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param kgKnowledge
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(KgKnowledge kgKnowledge) {
		super.save(kgKnowledge);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(kgKnowledge.getId(), "kgKnowledge_image");
		// 保存上传附件
		FileUploadUtils.saveFileUpload(kgKnowledge.getId(), "kgKnowledge_file");
	}
	
	/**
	 * 更新状态
	 * @param kgKnowledge
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(KgKnowledge kgKnowledge) {
		super.updateStatus(kgKnowledge);
	}
	
	/**
	 * 删除数据
	 * @param kgKnowledge
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(KgKnowledge kgKnowledge) {
		super.delete(kgKnowledge);
	}
	
}