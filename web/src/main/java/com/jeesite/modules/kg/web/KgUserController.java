/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.kg.entity.KgUser;
import com.jeesite.modules.kg.service.KgUserService;

/**
 * kg_userController
 * @author XJ
 * @version 2024-02-22
 */
@Controller
@RequestMapping(value = "${adminPath}/kg/kgUser")
public class KgUserController extends BaseController {

	@Autowired
	private KgUserService kgUserService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public KgUser get(String id, boolean isNewRecord) {
		return kgUserService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("kg:kgUser:view")
	@RequestMapping(value = {"list", ""})
	public String list(KgUser kgUser, Model model) {
		model.addAttribute("kgUser", kgUser);
		return "modules/kg/kgUserList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("kg:kgUser:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<KgUser> listData(KgUser kgUser, HttpServletRequest request, HttpServletResponse response) {
		kgUser.setPage(new Page<>(request, response));
		Page<KgUser> page = kgUserService.findPage(kgUser);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("kg:kgUser:view")
	@RequestMapping(value = "form")
	public String form(KgUser kgUser, Model model) {
		model.addAttribute("kgUser", kgUser);
		return "modules/kg/kgUserForm";
	}

	/**
	 * 保存kg_user
	 */
	@RequiresPermissions("kg:kgUser:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated KgUser kgUser) {
		kgUserService.save(kgUser);
		return renderResult(Global.TRUE, text("保存kg_user成功！"));
	}
	
	/**
	 * 停用kg_user
	 */
	@RequiresPermissions("kg:kgUser:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(KgUser kgUser) {
		kgUser.setStatus(KgUser.STATUS_DISABLE);
		kgUserService.updateStatus(kgUser);
		return renderResult(Global.TRUE, text("停用kg_user成功"));
	}
	
	/**
	 * 启用kg_user
	 */
	@RequiresPermissions("kg:kgUser:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(KgUser kgUser) {
		kgUser.setStatus(KgUser.STATUS_NORMAL);
		kgUserService.updateStatus(kgUser);
		return renderResult(Global.TRUE, text("启用kg_user成功"));
	}
	
	/**
	 * 删除kg_user
	 */
	@RequiresPermissions("kg:kgUser:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(KgUser kgUser) {
		kgUserService.delete(kgUser);
		return renderResult(Global.TRUE, text("删除kg_user成功！"));
	}
	
}