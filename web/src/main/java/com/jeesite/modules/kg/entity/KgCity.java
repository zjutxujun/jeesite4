/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * kg_cityEntity
 * @author XJ
 * @version 2024-02-22
 */
@Table(name="kg_city", alias="a", columns={
		@Column(name="node", attrName="node", label="node"),
		@Column(name="nodeid", attrName="nodeid", label="nodeid", isPK=true),
		@Column(name="name", attrName="name", label="name", queryType=QueryType.LIKE),
		@Column(name="delflag", attrName="delflag", label="delflag"),
		@Column(name="code", attrName="code", label="code"),
		@Column(name="property", attrName="property", label="property"),
	}, orderBy="a.nodeid DESC"
)
public class KgCity extends DataEntity<KgCity> {
	
	private static final long serialVersionUID = 1L;
	private String node;		// node
	private String nodeid;		// nodeid
	private String name;		// name
	private String delflag;		// delflag
	private String code;		// code
	private String property;		// property
	
	public KgCity() {
		this(null);
	}

	public KgCity(String id){
		super(id);
	}
	
	@Length(min=0, max=255, message="node长度不能超过 255 个字符")
	public String getNode() {
		return node;
	}

	public void setNode(String node) {
		this.node = node;
	}
	
	public String getNodeid() {
		return nodeid;
	}

	public void setNodeid(String nodeid) {
		this.nodeid = nodeid;
	}
	
	@Length(min=0, max=255, message="name长度不能超过 255 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=255, message="delflag长度不能超过 255 个字符")
	public String getDelflag() {
		return delflag;
	}

	public void setDelflag(String delflag) {
		this.delflag = delflag;
	}
	
	@Length(min=0, max=255, message="code长度不能超过 255 个字符")
	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	
	@Length(min=0, max=255, message="property长度不能超过 255 个字符")
	public String getProperty() {
		return property;
	}

	public void setProperty(String property) {
		this.property = property;
	}
	
}