/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.kg.entity.KgDigital;
import com.jeesite.modules.kg.service.KgDigitalService;

/**
 * kg_digitalController
 * @author XJ
 * @version 2024-02-22
 */
@Controller
@RequestMapping(value = "${adminPath}/kg/kgDigital")
public class KgDigitalController extends BaseController {

	@Autowired
	private KgDigitalService kgDigitalService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public KgDigital get(String id, boolean isNewRecord) {
		return kgDigitalService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("kg:kgDigital:view")
	@RequestMapping(value = {"list", ""})
	public String list(KgDigital kgDigital, Model model) {
		model.addAttribute("kgDigital", kgDigital);
		return "modules/kg/kgDigitalList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("kg:kgDigital:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<KgDigital> listData(KgDigital kgDigital, HttpServletRequest request, HttpServletResponse response) {
		kgDigital.setPage(new Page<>(request, response));
		Page<KgDigital> page = kgDigitalService.findPage(kgDigital);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("kg:kgDigital:view")
	@RequestMapping(value = "form")
	public String form(KgDigital kgDigital, Model model) {
		model.addAttribute("kgDigital", kgDigital);
		return "modules/kg/kgDigitalForm";
	}

	/**
	 * 保存kg_digital
	 */
	@RequiresPermissions("kg:kgDigital:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated KgDigital kgDigital) {
		kgDigitalService.save(kgDigital);
		return renderResult(Global.TRUE, text("保存kg_digital成功！"));
	}
	
	/**
	 * 停用kg_digital
	 */
	@RequiresPermissions("kg:kgDigital:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(KgDigital kgDigital) {
		kgDigital.setStatus(KgDigital.STATUS_DISABLE);
		kgDigitalService.updateStatus(kgDigital);
		return renderResult(Global.TRUE, text("停用kg_digital成功"));
	}
	
	/**
	 * 启用kg_digital
	 */
	@RequiresPermissions("kg:kgDigital:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(KgDigital kgDigital) {
		kgDigital.setStatus(KgDigital.STATUS_NORMAL);
		kgDigitalService.updateStatus(kgDigital);
		return renderResult(Global.TRUE, text("启用kg_digital成功"));
	}
	
	/**
	 * 删除kg_digital
	 */
	@RequiresPermissions("kg:kgDigital:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(KgDigital kgDigital) {
		kgDigitalService.delete(kgDigital);
		return renderResult(Global.TRUE, text("删除kg_digital成功！"));
	}
	
}