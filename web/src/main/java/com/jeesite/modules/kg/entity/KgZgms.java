/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * kg_zgmsEntity
 * @author XJ
 * @version 2024-02-22
 */
@Table(name="kg_zgms", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="kg_id", attrName="kgId", label="kg_id"),
		@Column(name="name", attrName="name", label="name", queryType=QueryType.LIKE),
		@Column(name="msdate", attrName="msdate", label="msdate"),
		@Column(name="info", attrName="info", label="info", queryType=QueryType.LIKE),
		@Column(name="json", attrName="json", label="json", queryType=QueryType.LIKE),
		@Column(name="type", attrName="type", label="type"),
		@Column(name="kg_name", attrName="kgName", label="kg_name", queryType=QueryType.LIKE),
		@Column(name="picture", attrName="picture", label="picture"),
		@Column(name="tag1", attrName="tag1", label="tag1", queryType=QueryType.LIKE),
		@Column(name="tag2", attrName="tag2", label="tag2", queryType=QueryType.LIKE),
		@Column(name="tag3", attrName="tag3", label="tag3", queryType=QueryType.LIKE),
		@Column(name="coll", attrName="coll", label="coll"),
		@Column(name="hit", attrName="hit", label="hit"),
		@Column(name="status", attrName="status", label="status", isUpdate=false),
		@Column(name="note", attrName="note", label="note"),
		@Column(name="remark", attrName="remark", label="remark"),
	}, orderBy="a.id DESC"
)
public class KgZgms extends DataEntity<KgZgms> {
	
	private static final long serialVersionUID = 1L;
	private String kgId;		// kg_id
	private String name;		// name
	private String msdate;		// msdate
	private String info;		// info
	private String json;		// json
	private String type;		// type
	private String kgName;		// kg_name
	private String picture;		// picture
	private String tag1;		// tag1
	private String tag2;		// tag2
	private String tag3;		// tag3
	private Long coll;		// coll
	private Long hit;		// hit
	private String note;		// note
	private String remark;		// remark
	
	public KgZgms() {
		this(null);
	}

	public KgZgms(String id){
		super(id);
	}
	
	@Length(min=0, max=20, message="kg_id长度不能超过 20 个字符")
	public String getKgId() {
		return kgId;
	}

	public void setKgId(String kgId) {
		this.kgId = kgId;
	}
	
	@Length(min=0, max=100, message="name长度不能超过 100 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=200, message="msdate长度不能超过 200 个字符")
	public String getMsdate() {
		return msdate;
	}

	public void setMsdate(String msdate) {
		this.msdate = msdate;
	}
	
	@Length(min=0, max=500, message="info长度不能超过 500 个字符")
	public String getInfo() {
		return info;
	}

	public void setInfo(String info) {
		this.info = info;
	}
	
	public String getJson() {
		return json;
	}

	public void setJson(String json) {
		this.json = json;
	}
	
	@Length(min=0, max=100, message="type长度不能超过 100 个字符")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Length(min=0, max=100, message="kg_name长度不能超过 100 个字符")
	public String getKgName() {
		return kgName;
	}

	public void setKgName(String kgName) {
		this.kgName = kgName;
	}
	
	@Length(min=0, max=200, message="picture长度不能超过 200 个字符")
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	@Length(min=0, max=200, message="tag1长度不能超过 200 个字符")
	public String getTag1() {
		return tag1;
	}

	public void setTag1(String tag1) {
		this.tag1 = tag1;
	}
	
	@Length(min=0, max=200, message="tag2长度不能超过 200 个字符")
	public String getTag2() {
		return tag2;
	}

	public void setTag2(String tag2) {
		this.tag2 = tag2;
	}
	
	@Length(min=0, max=200, message="tag3长度不能超过 200 个字符")
	public String getTag3() {
		return tag3;
	}

	public void setTag3(String tag3) {
		this.tag3 = tag3;
	}
	
	public Long getColl() {
		return coll;
	}

	public void setColl(Long coll) {
		this.coll = coll;
	}
	
	public Long getHit() {
		return hit;
	}

	public void setHit(Long hit) {
		this.hit = hit;
	}
	
	@Length(min=0, max=200, message="note长度不能超过 200 个字符")
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
	@Length(min=0, max=200, message="remark长度不能超过 200 个字符")
	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	public String getMsdate_gte() {
		return sqlMap.getWhere().getValue("msdate", QueryType.GTE);
	}

	public void setMsdate_gte(String msdate) {
		sqlMap.getWhere().and("msdate", QueryType.GTE, msdate);
	}
	
	public String getMsdate_lte() {
		return sqlMap.getWhere().getValue("msdate", QueryType.LTE);
	}

	public void setMsdate_lte(String msdate) {
		sqlMap.getWhere().and("msdate", QueryType.LTE, msdate);
	}
	
}