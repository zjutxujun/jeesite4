/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.kg.entity.KgCity;
import com.jeesite.modules.kg.dao.KgCityDao;

/**
 * kg_cityService
 * @author XJ
 * @version 2024-02-22
 */
@Service
@Transactional(readOnly=true)
public class KgCityService extends CrudService<KgCityDao, KgCity> {
	
	/**
	 * 获取单条数据
	 * @param kgCity
	 * @return
	 */
	@Override
	public KgCity get(KgCity kgCity) {
		return super.get(kgCity);
	}
	
	/**
	 * 查询分页数据
	 * @param kgCity 查询条件
	 * @param kgCity.page 分页对象
	 * @return
	 */
	@Override
	public Page<KgCity> findPage(KgCity kgCity) {
		return super.findPage(kgCity);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param kgCity
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(KgCity kgCity) {
		super.save(kgCity);
	}
	
	/**
	 * 更新状态
	 * @param kgCity
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(KgCity kgCity) {
		super.updateStatus(kgCity);
	}
	
	/**
	 * 删除数据
	 * @param kgCity
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(KgCity kgCity) {
		super.delete(kgCity);
	}
	
}