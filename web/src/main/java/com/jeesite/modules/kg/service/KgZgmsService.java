/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.kg.entity.KgZgms;
import com.jeesite.modules.kg.dao.KgZgmsDao;
import com.jeesite.modules.file.utils.FileUploadUtils;

/**
 * kg_zgmsService
 * @author XJ
 * @version 2024-02-22
 */
@Service
@Transactional(readOnly=true)
public class KgZgmsService extends CrudService<KgZgmsDao, KgZgms> {
	
	/**
	 * 获取单条数据
	 * @param kgZgms
	 * @return
	 */
	@Override
	public KgZgms get(KgZgms kgZgms) {
		return super.get(kgZgms);
	}
	
	/**
	 * 查询分页数据
	 * @param kgZgms 查询条件
	 * @param kgZgms.page 分页对象
	 * @return
	 */
	@Override
	public Page<KgZgms> findPage(KgZgms kgZgms) {
		return super.findPage(kgZgms);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param kgZgms
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(KgZgms kgZgms) {
		super.save(kgZgms);
		// 保存上传图片
		FileUploadUtils.saveFileUpload(kgZgms.getId(), "kgZgms_image");
	}
	
	/**
	 * 更新状态
	 * @param kgZgms
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(KgZgms kgZgms) {
		super.updateStatus(kgZgms);
	}
	
	/**
	 * 删除数据
	 * @param kgZgms
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(KgZgms kgZgms) {
		super.delete(kgZgms);
	}
	
}