/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.web;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.jeesite.common.config.Global;
import com.jeesite.common.entity.Page;
import com.jeesite.common.web.BaseController;
import com.jeesite.modules.kg.entity.KgKnowledge;
import com.jeesite.modules.kg.service.KgKnowledgeService;

/**
 * kg_knowledgeController
 * @author XJ
 * @version 2024-02-22
 */
@Controller
@RequestMapping(value = "${adminPath}/kg/kgKnowledge")
public class KgKnowledgeController extends BaseController {

	@Autowired
	private KgKnowledgeService kgKnowledgeService;
	
	/**
	 * 获取数据
	 */
	@ModelAttribute
	public KgKnowledge get(String id, boolean isNewRecord) {
		return kgKnowledgeService.get(id, isNewRecord);
	}
	
	/**
	 * 查询列表
	 */
	@RequiresPermissions("kg:kgKnowledge:view")
	@RequestMapping(value = {"list", ""})
	public String list(KgKnowledge kgKnowledge, Model model) {
		model.addAttribute("kgKnowledge", kgKnowledge);
		return "modules/kg/kgKnowledgeList";
	}
	
	/**
	 * 查询列表数据
	 */
	@RequiresPermissions("kg:kgKnowledge:view")
	@RequestMapping(value = "listData")
	@ResponseBody
	public Page<KgKnowledge> listData(KgKnowledge kgKnowledge, HttpServletRequest request, HttpServletResponse response) {
		kgKnowledge.setPage(new Page<>(request, response));
		Page<KgKnowledge> page = kgKnowledgeService.findPage(kgKnowledge);
		return page;
	}

	/**
	 * 查看编辑表单
	 */
	@RequiresPermissions("kg:kgKnowledge:view")
	@RequestMapping(value = "form")
	public String form(KgKnowledge kgKnowledge, Model model) {
		model.addAttribute("kgKnowledge", kgKnowledge);
		return "modules/kg/kgKnowledgeForm";
	}

	/**
	 * 保存kg_knowledge
	 */
	@RequiresPermissions("kg:kgKnowledge:edit")
	@PostMapping(value = "save")
	@ResponseBody
	public String save(@Validated KgKnowledge kgKnowledge) {
		kgKnowledgeService.save(kgKnowledge);
		return renderResult(Global.TRUE, text("保存kg_knowledge成功！"));
	}
	
	/**
	 * 停用kg_knowledge
	 */
	@RequiresPermissions("kg:kgKnowledge:edit")
	@RequestMapping(value = "disable")
	@ResponseBody
	public String disable(KgKnowledge kgKnowledge) {
		kgKnowledge.setStatus(KgKnowledge.STATUS_DISABLE);
		kgKnowledgeService.updateStatus(kgKnowledge);
		return renderResult(Global.TRUE, text("停用kg_knowledge成功"));
	}
	
	/**
	 * 启用kg_knowledge
	 */
	@RequiresPermissions("kg:kgKnowledge:edit")
	@RequestMapping(value = "enable")
	@ResponseBody
	public String enable(KgKnowledge kgKnowledge) {
		kgKnowledge.setStatus(KgKnowledge.STATUS_NORMAL);
		kgKnowledgeService.updateStatus(kgKnowledge);
		return renderResult(Global.TRUE, text("启用kg_knowledge成功"));
	}
	
	/**
	 * 删除kg_knowledge
	 */
	@RequiresPermissions("kg:kgKnowledge:edit")
	@RequestMapping(value = "delete")
	@ResponseBody
	public String delete(KgKnowledge kgKnowledge) {
		kgKnowledgeService.delete(kgKnowledge);
		return renderResult(Global.TRUE, text("删除kg_knowledge成功！"));
	}
	
}