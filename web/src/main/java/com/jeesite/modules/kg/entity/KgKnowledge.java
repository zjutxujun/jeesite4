/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * kg_knowledgeEntity
 * @author XJ
 * @version 2024-02-22
 */
@Table(name="kg_knowledge", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="kg_id", attrName="kgId", label="kg_id"),
		@Column(name="name", attrName="name", label="name", queryType=QueryType.LIKE),
		@Column(name="tag", attrName="tag", label="标签", queryType=QueryType.LIKE),
		@Column(name="content", attrName="content", label="content", queryType=QueryType.LIKE),
		@Column(name="type", attrName="type", label="type"),
		@Column(name="kg_name", attrName="kgName", label="kg_name", queryType=QueryType.LIKE),
		@Column(name="picture", attrName="picture", label="首页图片"),
		@Column(name="coll", attrName="coll", label="收藏"),
		@Column(name="hit", attrName="hit", label="点击率"),
		@Column(name="status", attrName="status", label="状态", comment="状态（0正常 1删除 2停用）", isUpdate=false),
		@Column(name="note", attrName="note", label="注意事项"),
	}, orderBy="a.id DESC"
)
public class KgKnowledge extends DataEntity<KgKnowledge> {
	
	private static final long serialVersionUID = 1L;
	private String kgId;		// kg_id
	private String name;		// name
	private String tag;		// 标签
	private String content;		// content
	private String type;		// type
	private String kgName;		// kg_name
	private String picture;		// 首页图片
	private Long coll;		// 收藏
	private Long hit;		// 点击率
	private String note;		// 注意事项
	
	public KgKnowledge() {
		this(null);
	}

	public KgKnowledge(String id){
		super(id);
	}
	
	@Length(min=0, max=20, message="kg_id长度不能超过 20 个字符")
	public String getKgId() {
		return kgId;
	}

	public void setKgId(String kgId) {
		this.kgId = kgId;
	}
	
	@Length(min=0, max=100, message="name长度不能超过 100 个字符")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	@Length(min=0, max=200, message="标签长度不能超过 200 个字符")
	public String getTag() {
		return tag;
	}

	public void setTag(String tag) {
		this.tag = tag;
	}
	
	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	
	@Length(min=0, max=100, message="type长度不能超过 100 个字符")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	@Length(min=0, max=100, message="kg_name长度不能超过 100 个字符")
	public String getKgName() {
		return kgName;
	}

	public void setKgName(String kgName) {
		this.kgName = kgName;
	}
	
	@Length(min=0, max=100, message="首页图片长度不能超过 100 个字符")
	public String getPicture() {
		return picture;
	}

	public void setPicture(String picture) {
		this.picture = picture;
	}
	
	public Long getColl() {
		return coll;
	}

	public void setColl(Long coll) {
		this.coll = coll;
	}
	
	public Long getHit() {
		return hit;
	}

	public void setHit(Long hit) {
		this.hit = hit;
	}
	
	@Length(min=0, max=100, message="注意事项长度不能超过 100 个字符")
	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}
	
}