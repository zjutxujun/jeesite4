/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.kg.entity.KgUser;
import com.jeesite.modules.kg.dao.KgUserDao;
import com.jeesite.modules.kg.entity.KgUserColl;
import com.jeesite.modules.kg.dao.KgUserCollDao;

/**
 * kg_userService
 * @author XJ
 * @version 2024-02-22
 */
@Service
@Transactional(readOnly=true)
public class KgUserService extends CrudService<KgUserDao, KgUser> {
	
	@Autowired
	private KgUserCollDao kgUserCollDao;
	
	/**
	 * 获取单条数据
	 * @param kgUser
	 * @return
	 */
	@Override
	public KgUser get(KgUser kgUser) {
		KgUser entity = super.get(kgUser);
		if (entity != null){
			KgUserColl kgUserColl = new KgUserColl(entity);
			kgUserColl.setStatus(KgUserColl.STATUS_NORMAL);
			entity.setKgUserCollList(kgUserCollDao.findList(kgUserColl));
		}
		return entity;
	}
	
	/**
	 * 查询分页数据
	 * @param kgUser 查询条件
	 * @param kgUser.page 分页对象
	 * @return
	 */
	@Override
	public Page<KgUser> findPage(KgUser kgUser) {
		return super.findPage(kgUser);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param kgUser
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(KgUser kgUser) {
		super.save(kgUser);
		// 保存 KgUser子表
		for (KgUserColl kgUserColl : kgUser.getKgUserCollList()){
			if (!KgUserColl.STATUS_DELETE.equals(kgUserColl.getStatus())){
				kgUserColl.setUid(kgUser);
				if (kgUserColl.getIsNewRecord()){
					kgUserCollDao.insert(kgUserColl);
				}else{
					kgUserCollDao.update(kgUserColl);
				}
			}else{
				kgUserCollDao.delete(kgUserColl);
			}
		}
	}
	
	/**
	 * 更新状态
	 * @param kgUser
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(KgUser kgUser) {
		super.updateStatus(kgUser);
	}
	
	/**
	 * 删除数据
	 * @param kgUser
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(KgUser kgUser) {
		super.delete(kgUser);
		KgUserColl kgUserColl = new KgUserColl();
		kgUserColl.setUid(kgUser);
		kgUserCollDao.deleteByEntity(kgUserColl);
	}
	
}