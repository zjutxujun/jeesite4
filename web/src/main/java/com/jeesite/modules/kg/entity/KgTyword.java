/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.entity;

import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * kg_tywordEntity
 * @author XJ
 * @version 2024-02-25
 */
@Table(name="kg_tyword", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="word", attrName="word", label="word"),
		@Column(name="tyword", attrName="tyword", label="tyword"),
	}, orderBy="a.id DESC"
)
public class KgTyword extends DataEntity<KgTyword> {
	
	private static final long serialVersionUID = 1L;
	private String word;		// word
	private String tyword;		// tyword
	
	public KgTyword() {
		this(null);
	}

	public KgTyword(String id){
		super(id);
	}
	
	@Length(min=0, max=20, message="word长度不能超过 20 个字符")
	public String getWord() {
		return word;
	}

	public void setWord(String word) {
		this.word = word;
	}
	
	@Length(min=0, max=500, message="tyword长度不能超过 500 个字符")
	public String getTyword() {
		return tyword;
	}

	public void setTyword(String tyword) {
		this.tyword = tyword;
	}
	
}