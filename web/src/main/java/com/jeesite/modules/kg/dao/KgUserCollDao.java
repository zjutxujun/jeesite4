/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.dao;

import com.jeesite.common.dao.CrudDao;
import com.jeesite.common.mybatis.annotation.MyBatisDao;
import com.jeesite.modules.kg.entity.KgUserColl;

/**
 * kg_userDAO接口
 * @author XJ
 * @version 2024-02-22
 */
@MyBatisDao
public interface KgUserCollDao extends CrudDao<KgUserColl> {
	
}