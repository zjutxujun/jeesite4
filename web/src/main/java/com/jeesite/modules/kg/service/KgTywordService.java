/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.service;

import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.jeesite.common.entity.Page;
import com.jeesite.common.service.CrudService;
import com.jeesite.modules.kg.entity.KgTyword;
import com.jeesite.modules.kg.dao.KgTywordDao;

/**
 * kg_tywordService
 * @author XJ
 * @version 2024-02-25
 */
@Service
@Transactional(readOnly=true)
public class KgTywordService extends CrudService<KgTywordDao, KgTyword> {
	
	/**
	 * 获取单条数据
	 * @param kgTyword
	 * @return
	 */
	@Override
	public KgTyword get(KgTyword kgTyword) {
		return super.get(kgTyword);
	}
	
	/**
	 * 查询分页数据
	 * @param kgTyword 查询条件
	 * @param kgTyword.page 分页对象
	 * @return
	 */
	@Override
	public Page<KgTyword> findPage(KgTyword kgTyword) {
		return super.findPage(kgTyword);
	}
	
	/**
	 * 保存数据（插入或更新）
	 * @param kgTyword
	 */
	@Override
	@Transactional(readOnly=false)
	public void save(KgTyword kgTyword) {
		super.save(kgTyword);
	}
	
	/**
	 * 更新状态
	 * @param kgTyword
	 */
	@Override
	@Transactional(readOnly=false)
	public void updateStatus(KgTyword kgTyword) {
		super.updateStatus(kgTyword);
	}
	
	/**
	 * 删除数据
	 * @param kgTyword
	 */
	@Override
	@Transactional(readOnly=false)
	public void delete(KgTyword kgTyword) {
		super.delete(kgTyword);
	}
	
}