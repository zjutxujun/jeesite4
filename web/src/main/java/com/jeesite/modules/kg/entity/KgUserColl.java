/**
 * Copyright (c) 2013-Now http://jeesite.com All rights reserved.
 */
package com.jeesite.modules.kg.entity;

import javax.validation.constraints.NotBlank;
import org.hibernate.validator.constraints.Length;

import com.jeesite.common.entity.DataEntity;
import com.jeesite.common.mybatis.annotation.Column;
import com.jeesite.common.mybatis.annotation.Table;
import com.jeesite.common.mybatis.mapper.query.QueryType;

/**
 * kg_userEntity
 * @author XJ
 * @version 2024-02-22
 */
@Table(name="kg_user_coll", alias="a", columns={
		@Column(name="id", attrName="id", label="id", isPK=true),
		@Column(name="uid", attrName="uid.id", label="用户id"),
		@Column(name="kid", attrName="kid", label="民俗知识数字资源id"),
		@Column(name="type", attrName="type", label="type"),
	}, orderBy="a.id ASC"
)
public class KgUserColl extends DataEntity<KgUserColl> {
	
	private static final long serialVersionUID = 1L;
	private KgUser uid;		// 用户id 父类
	private String kid;		// 民俗知识数字资源id
	private String type;		// type
	
	public KgUserColl() {
		this(null);
	}


	public KgUserColl(KgUser uid){
		this.uid = uid;
	}
	
	@NotBlank(message="用户id不能为空")
	@Length(min=0, max=20, message="用户id长度不能超过 20 个字符")
	public KgUser getUid() {
		return uid;
	}

	public void setUid(KgUser uid) {
		this.uid = uid;
	}
	
	@Length(min=0, max=20, message="民俗知识数字资源id长度不能超过 20 个字符")
	public String getKid() {
		return kid;
	}

	public void setKid(String kid) {
		this.kid = kid;
	}
	
	@Length(min=0, max=100, message="type长度不能超过 100 个字符")
	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
}